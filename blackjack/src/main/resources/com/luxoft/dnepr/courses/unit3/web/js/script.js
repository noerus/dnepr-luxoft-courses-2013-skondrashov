var SERVICE_URL = "/service";

var $card = $("<div>").addClass("card");
var $suit = $("<div>").addClass("suit");

var state;

$(document).ready(function(){
    $.ajaxSetup({cache: false});
    newGame();
})

function newGame() {
    state = null;
    setState(state);
    $.getJSON(SERVICE_URL, {method: "newGame"}, function(json) {
        setCards("dealer", json.dealersHand);
        setCards("player", json.myHand);
    });
}

function hit() {
    if (state == null) {
        $.getJSON(SERVICE_URL, {method: "requestMore"}, function(json) {
            setCards("player", json.myHand);
            if (json.result === false) {
                state = "LOOSE";
                setState(state);
            }
        });
    }
}

function stay() {
    if (state == null) {
        $.getJSON(SERVICE_URL, {method: "requestStop"}, function(json) {
            state = json.state;
            setCards("dealer", json.dealersHand);
            setState(state);
        });
    }
}

function setCards(target, hand) {
    $("#"+target+"Hand").empty();

    for (i in hand.cards) {
        $("#"+target+"Hand").append(
            $card.clone().html(hand.cards[i].rank).append(
                $suit.clone().html(getSuit(hand.cards[i].suit))
            ).addClass(hand.cards[i].suit)
        );
    }

    $("#"+target+"Total").html("<b>" + hand.total + "</b> points");
}

function getSuit(suit) {
    switch (suit) {
        case 'SPADES': return "&spades;";
        case 'HEARTS': return "&hearts;";
        case 'DIAMONDS': return "&diams;";
        case 'CLUBS': return "&clubs;";
    }
}

function setState(s) {
    var mess;
    switch (s) {
        case 'WIN':
            mess = 'Congrats! You win!';
            $("#state").css({'color':'red'});
            break;
        case 'LOOSE':
            mess = 'Sorry, today is not your day. You loose.';
            $("#state").css({'color':'black'});
            break;
        case 'PUSH':
            mess = 'Push. Everybody has equal amount of points.';
            $("#state").css({'color':'blue'});
            break;
    }
    if (state == null) {
        $("#state").empty();
    } else {
        $("#state").html(mess);
    }
}