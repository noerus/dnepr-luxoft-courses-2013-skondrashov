package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import java.util.ArrayList;
import java.util.List;

public final class Deck {
    public static final int BLACKJACK = 21;

    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        if (size < 1) {
            size = 1;
        } else if (size > 10) {
            size = 10;
        }

        List<Card> cardList = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    cardList.add(new Card(rank, suit));
                }
            }
        }

        return cardList;
    }

    public static int costOf(List<Card> hand) {
        int sum = 0;
        int aces = 0;

        for (Card card : hand) {
            sum += card.getCost();
            if (card.getRank() == Rank.RANK_ACE) aces++;
        }

        while (sum > BLACKJACK && aces > 0) {
            sum -= 10;
            aces--;
        }

        return sum;
    }
}
