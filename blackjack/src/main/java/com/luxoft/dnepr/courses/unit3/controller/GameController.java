package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static com.luxoft.dnepr.courses.unit3.controller.Deck.*;

public class GameController {
    private static final int NUMBER_OF_DECKS = 6;

    private static GameController controller;

    private GameController() {
        player = new ArrayList<>();
        dealer = new ArrayList<>();
        deck = createDeck(NUMBER_OF_DECKS);
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }


    private List<Card> player;
    private List<Card> dealer;
    private List<Card> deck;
    private Iterator<Card> deckTop;


    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту дилеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        player.clear();
        dealer.clear();
        shuffler.shuffle(deck);
        deckTop = deck.iterator();


        player.add(getCardFromDeck());
        player.add(getCardFromDeck());
        dealer.add(getCardFromDeck());
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if (costOf(player) > BLACKJACK || !deckTop.hasNext()) return false;

        player.add(getCardFromDeck());
        if (costOf(player) <= BLACKJACK) return true;
        return false;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (дилер).
     * Сдаем дилеру карты пока у дилера не наберется 17 очков.
     */
    public void requestStop() {
        while (costOf(dealer) < 17) {
            dealer.add(getCardFromDeck());
        }
    }

    /**
     * Сравниваем руку дилера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у дилера и у дилера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у дилера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        int pl = costOf(player);
        int de = costOf(dealer);

        if (pl > BLACKJACK) return WinState.LOOSE;

        if (pl < de && de <= BLACKJACK) return WinState.LOOSE;

        if (pl == de) return WinState.PUSH;

        if (pl > de || de > BLACKJACK) return WinState.WIN;

        return null;
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return player;
    }

    /**
     * Возвращаем руку дилера
     */
    public List<Card> getDealersHand() {
        return dealer;
    }

    private Card getCardFromDeck() {
        if (deckTop.hasNext()) {
            return deckTop.next();
        } else {
            throw new RuntimeException("Deck is empty");
        }
    }
}
