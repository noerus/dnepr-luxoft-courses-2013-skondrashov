package com.luxoft.dnepr.courses.unit3.view;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import static com.luxoft.dnepr.courses.unit3.controller.Deck.costOf;

public class CommandLineInterface {
    private static final String FINAL_WIN = "Congrats! You win!";
    private static final String FINAL_PUSH = "Push. Everybody has equal amount of points.";
    private static final String FINAL_LOOSE = "Sorry, today is not your day. You loose.";

    public static boolean loop = false; // auto start a new game

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: Sergey Kondrashov\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        do {
            controller.newGame();

            printState(controller);

            while (scanner.hasNext()) {
                String command = scanner.next();
                if (!execute(command, controller)) {
                    break;
                }
            }
        } while (loop);
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет дилер (GameController.requestStop())
     *      после того как дилер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука дилера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        switch (command.toLowerCase()) {
            case Command.HELP:
                printHelp();
                break;

            case Command.MORE:
                if (!doMore(controller))
                    return false;
                break;

            case Command.STOP:
                doStop(controller);
                return false;

            case Command.EXIT:
                loop = false;
                return false;

            default:
                output.println("Invalid command");
        }

        return true;
    }

    private boolean doMore(GameController controller) {
        boolean more = controller.requestMore();

        printState(controller);

        if (!more) {
            output.println();
            output.println(FINAL_LOOSE);
            return false;
        }

        return true;
    }

    private void doStop(GameController controller) {
        controller.requestStop();

        output.println("Dealer turn:");
        output.println();

        printState(controller);
        output.println();

        switch (controller.getWinState()) {
            case WIN:
                output.println(FINAL_WIN);
                break;
            case PUSH:
                output.println(FINAL_PUSH);
                break;
            case LOOSE:
                output.println(FINAL_LOOSE);
                break;
        }
    }

    private void printHelp() {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        List<Card> dealersHand = controller.getDealersHand();

        output.println(printCards(myHand) + "(total " + costOf(myHand) + ")");
        output.println(printCards(dealersHand) + "(total " + costOf(dealersHand) + ")");
    }

    private final StringBuilder stringBuilder = new StringBuilder();

    private String printCards(List<Card> hand) {
        stringBuilder.setLength(0);

        for (Card card : hand) {
            stringBuilder.append(card.getRank().getName());
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();
    }
}
