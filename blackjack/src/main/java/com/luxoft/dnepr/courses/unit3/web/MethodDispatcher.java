package com.luxoft.dnepr.courses.unit3.web;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

import java.util.List;

public class MethodDispatcher {

    /**
     * @param request
     * @param response
     * @return response or <code>null</code> if wasn't able to find a method.
     */
    public Response dispatch(Request request, Response response) {
        String method = request.getParameters().get("method");
        if (method == null) {
            return null;
        } else if (method.equals("requestMore")) {
            return requestMore(response);
        } else if (method.equals("newGame")) {
            return newGame(response);
        } else if (method.equals("requestStop")) {
            return requestStop(response);
        }
        return null;
    }

    private Response newGame(Response response) {
        GameController.getInstance().newGame();
        response.write("{\"myHand\":");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(",\"dealersHand\":");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");
        return response;
    }

    private Response requestMore(Response response) {
        response.write("{\"result\":" + GameController.getInstance().requestMore());
        response.write(",\"myHand\":");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write("}");
        return response;
    }

    private Response requestStop(Response response) {
        GameController.getInstance().requestStop();
        response.write("{\"state\":\"" + GameController.getInstance().getWinState().toString() + "\"");
        response.write(",\"dealersHand\":");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");
        return response;
    }

    private void writeHand(Response response, List<Card> hand) {
        boolean isFirst = true;
        response.write("{\"cards\":[");
        for (Card card : hand) {
            if (isFirst) {
                isFirst = false;
            } else {
                response.write(",");
            }
            response.write("{\"rank\":\"");
            response.write(card.getRank().getName());
            response.write("\",\"suit\":\"");
            response.write(card.getSuit().name());
            response.write("\"}");
        }
        response.write("],\"total\":");
        response.write(String.valueOf(Deck.costOf(hand)));
        response.write("}");
    }
}
