<%@page import="com.luxoft.dnepr.courses.unit13.Util"%>
<%
    long activeUsers = ((Number) application.getAttribute(Util.ACTIVE_SESSIONS_USER)).longValue();
    long activeAdmins = ((Number) application.getAttribute(Util.ACTIVE_SESSIONS_ADMIN)).longValue();
    long activeTotal = activeUsers + activeAdmins;
    long requestsTotal = ((Number) application.getAttribute(Util.HTTP_REQUESTS)).longValue();
    long requestsPost = ((Number) application.getAttribute(Util.HTTP_REQUESTS_POST)).longValue();
    long requestsGet = ((Number) application.getAttribute(Util.HTTP_REQUESTS_GET)).longValue();
    long requestsOther = ((Number) application.getAttribute(Util.HTTP_REQUESTS_OTHER)).longValue();
%>
<!DOCTYPE html>
<html>
<body>
<center>
    <table border="1">
        <tr>
            <th>Parameter</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Active Sessions</td>
            <td><%= activeTotal %></td>
        </tr>
        <tr>
            <td>Active Sessions (ROLE user)</td>
            <td><%= activeUsers %></td>
        </tr>
        <tr>
            <td>Active Sessions (ROLE admin)</td>
            <td><%= activeAdmins %></td>
        </tr>
        <tr>
            <td>Total Count of HttpRequests</td>
            <td><%= requestsTotal %></td>
        </tr>
        <tr>
            <td>Total Count of POST HttpRequests</td>
            <td><%= requestsPost %></td>
        </tr>
        <tr>
            <td>Total Count of GET HttpRequests</td>
            <td><%= requestsGet %></td>
        </tr>
        <tr>
            <td>Total Count of Other HttpRequests</td>
            <td><%= requestsOther %></td>
        </tr>
    </table>
</center>
</body>
</html>