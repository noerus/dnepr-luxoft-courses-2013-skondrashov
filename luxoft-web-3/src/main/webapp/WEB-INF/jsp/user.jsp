<!DOCTYPE html>
<html>
<body>
<center>
    <a href="logout">logout</a>
    <h1>Hello <%= session.getAttribute("login") %>!</h1>
    <%
        String role = (String) session.getAttribute("role");
        if (role.equals("admin")) out.println("<a href=\"admin/sessionData\">Session Data</a>");
    %>
</center>
</body>
</html>