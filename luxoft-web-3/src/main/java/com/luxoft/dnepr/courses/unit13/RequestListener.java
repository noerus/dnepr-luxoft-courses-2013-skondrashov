package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

public class RequestListener implements ServletRequestListener {
    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        ServletContext sc = sre.getServletContext();

        ((AtomicLong) sc.getAttribute(Util.HTTP_REQUESTS)).incrementAndGet();

        switch (((HttpServletRequest) sre.getServletRequest()).getMethod()) {
            case "POST":
                ((AtomicLong) sc.getAttribute(Util.HTTP_REQUESTS_POST)).incrementAndGet();
                break;
            case "GET":
                ((AtomicLong) sc.getAttribute(Util.HTTP_REQUESTS_GET)).incrementAndGet();
                break;
            default:
                ((AtomicLong) sc.getAttribute(Util.HTTP_REQUESTS_OTHER)).incrementAndGet();
        }
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
    }
}
