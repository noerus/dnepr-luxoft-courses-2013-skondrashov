package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class AuthServlet extends HttpServlet {
    private static final Cookie WRONG_LOGIN_OR_PASS = new Cookie("error", "Wrong login or password");

    //login
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!req.getServletPath().equals("/login")) return;
        ServletContext sc = getServletContext();


        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role;

        Map<String, String> passwords = (Map) sc.getAttribute("passwords");
        Map<String, String> roles = (Map) sc.getAttribute("roles");

        if (passwords.containsKey(login) && Objects.equals(password, passwords.get(login))) {
            role = roles.get(login);

            HttpSession session = req.getSession(true);
            if (session.getAttribute("login") != null) {
                switch ((String) session.getAttribute("role")) {
                    case "user":
                        ((AtomicLong) sc.getAttribute(Util.ACTIVE_SESSIONS_USER)).decrementAndGet();
                        break;
                    case "admin":
                        ((AtomicLong) sc.getAttribute(Util.ACTIVE_SESSIONS_ADMIN)).decrementAndGet();
                        break;
                }
            }
            synchronized (session) {
                session.setAttribute("login", login);
                session.setAttribute("role", role);
            }
            switch (role) {
                case "user":
                    ((AtomicLong) sc.getAttribute(Util.ACTIVE_SESSIONS_USER)).incrementAndGet();
                    break;
                case "admin":
                    ((AtomicLong) sc.getAttribute(Util.ACTIVE_SESSIONS_ADMIN)).incrementAndGet();
                    break;
            }

            resp.sendRedirect(req.getContextPath() + "/user");
        } else {
            WRONG_LOGIN_OR_PASS.setPath(req.getContextPath());
            resp.addCookie(WRONG_LOGIN_OR_PASS);
            resp.sendRedirect(req.getContextPath() + "/");
        }
    }

    //logout
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!req.getServletPath().equals("/logout")) return;

        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
