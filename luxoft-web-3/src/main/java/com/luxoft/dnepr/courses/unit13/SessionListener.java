package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ServletContext sc = se.getSession().getServletContext();

        String role = (String) se.getSession().getAttribute("role");
        if (role != null) {
            switch (role) {
                case "user":
                    ((AtomicLong) sc.getAttribute(Util.ACTIVE_SESSIONS_USER)).decrementAndGet();
                    break;
                case "admin":
                    ((AtomicLong) sc.getAttribute(Util.ACTIVE_SESSIONS_ADMIN)).decrementAndGet();
                    break;
            }
        }
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
    }
}
