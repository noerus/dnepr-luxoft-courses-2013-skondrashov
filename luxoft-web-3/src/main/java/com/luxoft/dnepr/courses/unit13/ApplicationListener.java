package com.luxoft.dnepr.courses.unit13;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();

        Map<String, String> passwords = new ConcurrentHashMap<>();
        Map<String, String> roles = new ConcurrentHashMap<>();
        sc.setAttribute("passwords", passwords);
        sc.setAttribute("roles", roles);

        InputStream xmlFile = sc.getResourceAsStream("/META-INF/" + sc.getInitParameter("users"));
        Document doc;
        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        NodeList users = doc.getDocumentElement().getElementsByTagName("user");
        for (int i = 0; i < users.getLength(); i++) {
            String name = ((Element) users.item(i)).getAttribute("name");
            String password = ((Element) users.item(i)).getAttribute("password");
            String role = ((Element) users.item(i)).getAttribute("role");

            passwords.put(name, password);
            roles.put(name, role);
        }

        sc.setAttribute(Util.ACTIVE_SESSIONS_USER, new AtomicLong(0));
        sc.setAttribute(Util.ACTIVE_SESSIONS_ADMIN, new AtomicLong(0));
        sc.setAttribute(Util.HTTP_REQUESTS, new AtomicLong(0));
        sc.setAttribute(Util.HTTP_REQUESTS_POST, new AtomicLong(0));
        sc.setAttribute(Util.HTTP_REQUESTS_GET, new AtomicLong(0));
        sc.setAttribute(Util.HTTP_REQUESTS_OTHER, new AtomicLong(0));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
