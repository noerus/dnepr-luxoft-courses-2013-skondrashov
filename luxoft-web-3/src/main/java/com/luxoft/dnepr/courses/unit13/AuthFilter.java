package com.luxoft.dnepr.courses.unit13;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class AuthFilter implements Filter {
    private static final Cookie LOG_IN = new Cookie("error", "Please, log in!");

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        Map<String, String> roles = (Map) req.getServletContext().getAttribute("roles");

        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("login") != null) {
            switch (req.getServletPath()) {
                case "/user":
                    chain.doFilter(request, response);
                    break;
                case "/admin/sessionData":
                    if (roles.get(session.getAttribute("login")).equals("admin")) {
                        chain.doFilter(request, response);
                    } else {
                        resp.sendRedirect(req.getContextPath() + "/user");
                    }
                    break;
            }
        } else {
            LOG_IN.setPath(req.getContextPath());
            resp.addCookie(LOG_IN);
            resp.sendRedirect(req.getContextPath() + "/");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
