package com.luxoft.dnepr.courses.unit13;

public class Util {
    public static final String ACTIVE_SESSIONS_USER = "counters.sessions.active.user";
    public static final String ACTIVE_SESSIONS_ADMIN = "counters.sessions.active.admin";
    public static final String HTTP_REQUESTS = "counters.requests.http";
    public static final String HTTP_REQUESTS_POST = "counters.requests.http.post";
    public static final String HTTP_REQUESTS_GET = "counters.requests.http.get";
    public static final String HTTP_REQUESTS_OTHER = "counters.requests.http.other";
}
