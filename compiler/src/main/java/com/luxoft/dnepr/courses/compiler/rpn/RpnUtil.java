package com.luxoft.dnepr.courses.compiler.rpn;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;
import com.luxoft.dnepr.courses.compiler.util.ExpressionUtil;
import com.luxoft.dnepr.courses.compiler.util.Token;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static com.luxoft.dnepr.courses.compiler.Compiler.addCommand;
import static com.luxoft.dnepr.courses.compiler.util.TokenType.OPEN;

/**
 * Reverse Polish Notation
 */
public final class RpnUtil {
    private RpnUtil() {
    }

    public static void process(String input, ByteArrayOutputStream stream) {
        List<Token> list = Token.stringToListOfToken(input);
        ExpressionUtil.checkConsistency(list);

        list = convertToRPN(list);
        fromTokensToCommands(list, stream);
    }

    /**
     * Method implements the Dijkstra's shunting-yard algorithm
     * <br/>
     * <a href="http://www.chris-j.co.uk/parsing.php">http://www.chris-j.co.uk/parsing.php</a>
     *
     * @param in input list
     * @return output list
     */
    public static List<Token> convertToRPN(List<Token> in) {
        List<Token> out = new ArrayList<>();
        Stack<Token> stack = new Stack<>();

        for (Token token : in) {
            switch (token.getType()) {
                case NUMBER:
                    out.add(token);
                    continue;
                case OPEN:
                    stack.push(token);
                    continue;
                case CLOSE:
                    while (stack.peek().getType() != OPEN) {
                        out.add(stack.pop());
                    }
                    stack.pop();
                    continue;
                default:
                    while (!stack.empty() && stack.peek().getPriority() >= token.getPriority()) {
                        out.add(stack.pop());
                    }
                    stack.push(token);
            }
        }

        while (!stack.empty()) {
            out.add(stack.pop());
        }

        return out;
    }

    public static void fromTokensToCommands(List<Token> list, ByteArrayOutputStream stream) {
        for (Token token : list) {
            switch (token.getType()) {
                case NUMBER:
                    addCommand(stream, VirtualMachine.PUSH, Double.parseDouble(token.getValue()));
                    break;
                case MUL:
                    addCommand(stream, VirtualMachine.MUL);
                    break;
                case DIV:
                    addCommand(stream, VirtualMachine.SWAP);
                    addCommand(stream, VirtualMachine.DIV);
                    break;
                case ADD:
                    addCommand(stream, VirtualMachine.ADD);
                    break;
                case SUB:
                    addCommand(stream, VirtualMachine.SWAP);
                    addCommand(stream, VirtualMachine.SUB);
                    break;
            }
        }
        addCommand(stream, VirtualMachine.PRINT);
    }
}
