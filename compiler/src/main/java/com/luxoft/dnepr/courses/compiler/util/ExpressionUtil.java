package com.luxoft.dnepr.courses.compiler.util;

import com.luxoft.dnepr.courses.compiler.CompilationException;

import java.util.List;

public final class ExpressionUtil {
    private static final String UNEXP = "Unexpected: ";
    private static final String DISBALANCED = "Parentheses are not balanced";
    private static final String BEGIN = "Unexpected begin of expression";
    private static final String END = "Unexpected end of expression";
    private static final String EMPTY = "Expression is empty";

    private ExpressionUtil() {
    }

    public static void checkConsistency(List<Token> list) {
        if (list.size() == 0) {
            throw new CompilationException(EMPTY);
        }

        TokenType previous = null;
        int bracketCount = 0;

        for (Token token : list) {
            if (previous == null) {
                switch (token.getType()) {
                    case OPEN:
                        bracketCount++;
                        break;
                    case NUMBER:
                        try {
                            Double.parseDouble(token.getValue());
                        } catch (NumberFormatException e) {
                            throw new CompilationException(token.getValue(), e);
                        }
                        break;
                    default:
                        throw new CompilationException(BEGIN);
                }

                previous = token.getType();
                continue;
            }

            switch (token.getType()) {
                case NUMBER:
                    switch (previous) {
                        case OPEN:
                        case MUL:
                        case DIV:
                        case ADD:
                        case SUB:
                            break;
                        default:
                            throw new CompilationException(UNEXP + token.getValue());
                    }

                    try {
                        Double.parseDouble(token.getValue());
                    } catch (NumberFormatException e) {
                        throw new CompilationException(token.getValue(), e);
                    }

                    break;
                case OPEN:
                    switch (previous) {
                        case MUL:
                        case DIV:
                        case ADD:
                        case SUB:
                        case OPEN:
                            break;
                        default:
                            throw new CompilationException(UNEXP + token.getValue());
                    }
                    bracketCount++;
                    break;
                case CLOSE:
                    switch (previous) {
                        case CLOSE:
                        case NUMBER:
                            break;
                        default:
                            throw new CompilationException(UNEXP + token.getValue());
                    }
                    bracketCount--;
                    if (bracketCount < 0) {
                        throw new CompilationException(DISBALANCED);
                    }
                    break;
                case MUL:
                case DIV:
                case ADD:
                case SUB:
                    switch (previous) {
                        case NUMBER:
                        case CLOSE:
                            break;
                        default:
                            throw new CompilationException(UNEXP + token.getValue());
                    }
                    break;
            }

            previous = token.getType();
        }

        if (bracketCount != 0) {
            throw new CompilationException(DISBALANCED);
        }

        switch (previous) {
            case NUMBER:
            case CLOSE:
                break;
            default:
                throw new CompilationException(END);
        }
    }
}
