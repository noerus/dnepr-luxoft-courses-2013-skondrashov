package com.luxoft.dnepr.courses.compiler.ast;

import com.luxoft.dnepr.courses.compiler.util.TokenType;

public class Node {
    Node parent;
    Node left;
    Node right;
    Node child;
    TokenType type;
    String number;
}
