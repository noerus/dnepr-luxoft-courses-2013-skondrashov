package com.luxoft.dnepr.courses.compiler.ast;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;
import com.luxoft.dnepr.courses.compiler.util.ExpressionUtil;
import com.luxoft.dnepr.courses.compiler.util.Token;
import com.luxoft.dnepr.courses.compiler.util.TokenType;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.luxoft.dnepr.courses.compiler.Compiler.addCommand;

public final class AstUtil {
    private AstUtil() {
    }

    public static void process(String input, ByteArrayOutputStream stream) {
        List<Token> list = Token.stringToListOfToken(input);
        ExpressionUtil.checkConsistency(list);

        Node curr = null;

        for (Token token : list) {
            TokenType tokenType = token.getType();

            Node n = new Node();
            n.type = tokenType;
            switch (tokenType) {
                case NUMBER:
                    n.number = token.getValue();

                    if (curr == null) {
                        curr = n;
                        break;
                    }

                    switch (curr.type) {
                        case OPEN:
                            curr.child = n;
                            curr.child.parent = curr;
                            curr = curr.child;
                            break;
                        case MUL:
                        case DIV:
                        case ADD:
                        case SUB:
                            curr.right = n;
                            curr.right.parent = curr;
                            curr = curr.right;
                            break;
                    }
                    break;
                case OPEN:
                    if (curr == null) {
                        curr = n;
                        break;
                    }

                    switch (curr.type) {
                        case MUL:
                        case DIV:
                        case ADD:
                        case SUB:
                            curr.right = n;
                            curr.right.parent = curr;
                            curr = curr.right;
                            break;
                        case OPEN:
                            curr.child = n;
                            curr.child.parent = curr;
                            curr = curr.child;
                            break;
                    }
                    break;
                case CLOSE:
                    do {
                        curr = curr.parent;
                    } while (curr.type != TokenType.OPEN);
                    break;
                default: // operation
                    if (curr.parent == null) {
                        curr.parent = n;
                        curr.parent.left = curr;
                        curr = curr.parent;
                        break;
                    } else {
                        switch (curr.parent.type) {
                            case OPEN:
                                n.parent = curr.parent;
                                n.left = curr;
                                curr.parent.child = n;
                                curr.parent = n;
                                curr = n;
                                break;
                            case ADD:
                            case SUB:
                            case MUL:
                            case DIV:
                                if (curr.parent.parent == null) {
                                    n.left = curr.parent;
                                    curr.parent.parent = n;
                                    curr = n;
                                } else {
                                    n.parent = curr.parent.parent;
                                    n.left = curr.parent;
                                    curr.parent.parent.child = n;
                                    curr.parent.parent = n;
                                    curr = n;
                                }
                                break;
                        }
                    }
            }
        }

        Node root;
        while (curr.parent != null) {
            curr = curr.parent;
        }
        root = curr;

        do {
            changed = false;
            recSwap(root);
            while (root.parent != null) {
                root = root.parent;
            }
        } while (changed);

        do {
            changed = false;
            root = recDelParentheses(root);
        } while (changed);


        recWalk(root, stream);
        addCommand(stream, VirtualMachine.PRINT);
    }

    private static boolean changed = false;

    private static void recWalk(Node root, ByteArrayOutputStream stream) {
        if (root == null) return;
        recWalk(root.right, stream);
        recWalk(root.left, stream);
        switch (root.type) {
            case NUMBER:
                addCommand(stream, VirtualMachine.PUSH, Double.parseDouble(root.number));
                break;
            case ADD:
                addCommand(stream, VirtualMachine.ADD);
                break;
            case SUB:
                addCommand(stream, VirtualMachine.SUB);
                break;
            case DIV:
                addCommand(stream, VirtualMachine.DIV);
                break;
            case MUL:
                addCommand(stream, VirtualMachine.MUL);
                break;
        }
    }

    private static Node recDelParentheses(Node root) {
        switch (root.type) {
            case NUMBER:
                return root;
            case OPEN:
                if (root.parent != null) {
                    if (root.parent.left == root) {
                        root.parent.left = root.child;
                    }
                    if (root.parent.right == root) {
                        root.parent.right = root.child;
                    }
                    if (root.parent.child == root) {
                        root.parent.child = root.child;
                    }
                }
                root.child.parent = root.parent;
                changed = true;
                return root.child;
            default:
                recDelParentheses(root.left);
                recDelParentheses(root.right);
        }
        return root;
    }

    private static void recSwap(Node node) {
        switch (node.type) {
            case NUMBER:
                return;
            case OPEN:
                recSwap(node.child);
                return;
            default: // operation
                recSwap(node.left);
                recSwap(node.right);
                if (node.parent == null) {
                    return;
                }
                if (node.parent.type.priority > node.type.priority) {
                    Node over = node.parent.parent;
                    Node top = node.parent;
                    Node bottom = node.right;

                    top.left = bottom;
                    bottom.parent = top;
                    node.right = top;
                    top.parent = node;
                    node.parent = over;
                    if (over != null) {
                        if (over.type == TokenType.OPEN) {
                            over.child = node;
                        } else {
                            over.left = node;
                        }
                    }

                    changed = true;
                }
        }
    }

}
