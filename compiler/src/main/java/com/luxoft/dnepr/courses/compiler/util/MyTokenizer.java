package com.luxoft.dnepr.courses.compiler.util;

import java.util.Iterator;
import java.util.StringTokenizer;

public class MyTokenizer extends StringTokenizer implements Iterable<String>, Iterator<String> {
    public MyTokenizer(String str, String delim, boolean returnDelims) {
        super(str, delim, returnDelims);
    }

    public MyTokenizer(String str, String delim) {
        super(str, delim);
    }

    public MyTokenizer(String str) {
        super(str);
    }

    @Override
    public Iterator<String> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return this.hasMoreTokens();
    }

    @Override
    public String next() {
        return this.nextToken();
    }

    @Override
    public void remove() {
        // do nothing
    }
}
