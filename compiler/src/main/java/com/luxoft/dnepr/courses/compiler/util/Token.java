package com.luxoft.dnepr.courses.compiler.util;

import java.util.ArrayList;
import java.util.List;

public class Token {
    private TokenType type;
    private String value;
    private int priority;

    public Token(TokenType type) {
        this.type = type;
        this.value = type.value;
        this.priority = type.priority;
    }

    public Token(String value) {
        this.type = TokenType.NUMBER;
        this.priority = TokenType.NUMBER.priority;
        this.value = value;
    }

    public TokenType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public int getPriority() {
        return priority;
    }

    public boolean isNumber() {
        return type == TokenType.NUMBER;
    }

    public static List<Token> stringToListOfToken(String str) {
        List<Token> list = new ArrayList<>();

        // preparing delimiters
        StringBuilder sb = new StringBuilder();
        for (TokenType type : TokenType.values()) {
            if (type == TokenType.NUMBER) continue;
            sb.append(type.value);
        }

        MyTokenizer tokens = new MyTokenizer(str, sb.toString(), true);

        for (String token : tokens) {
            boolean delimiter = false;

            for (TokenType type : TokenType.values()) {
                if (type == TokenType.NUMBER) continue;
                if (token.equals(type.value)) {
                    list.add(new Token(type));
                    delimiter = true;
                    break;
                }
            }

            if (!delimiter) {
                String number = token.trim();
                if (!number.isEmpty())
                    list.add(new Token(number));
            }
        }

        return list;
    }
}
