package com.luxoft.dnepr.courses.compiler.recursion;

import com.luxoft.dnepr.courses.compiler.VirtualMachine;
import com.luxoft.dnepr.courses.compiler.util.ExpressionUtil;
import com.luxoft.dnepr.courses.compiler.util.Token;
import com.luxoft.dnepr.courses.compiler.util.TokenType;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.luxoft.dnepr.courses.compiler.Compiler.addCommand;

public final class RecursionUtil {
    private static final int MIN_PRIORITY = 1;

    private static ByteArrayOutputStream stream;
    private static List<Token> list;

    private RecursionUtil() {
    }

    public static void process(String input, ByteArrayOutputStream stream) {
        RecursionUtil.stream = stream;

        list = Token.stringToListOfToken(input);
        ExpressionUtil.checkConsistency(list);

        recursion(0, list.size() - 1, MIN_PRIORITY);
        addCommand(stream, VirtualMachine.PRINT);
    }

    private static void recursion(int start, int end, int priority) {
        if (start == end) {
            double d = Double.parseDouble(list.get(start).getValue());
            addCommand(stream, VirtualMachine.PUSH, d);
            return;
        }

        if (!haveOutsideOperator(start, end)) {
            recursion(start + 1, end - 1, MIN_PRIORITY);
            return;
        }

        int bracketCount = 0;
        for (int i = end; i >= start; i--) {    // Important: from right to left!
            TokenType type = list.get(i).getType();

            switch (type) {
                case NUMBER:
                    continue;
                case CLOSE:
                    bracketCount++;
                    continue;
                case OPEN:
                    bracketCount--;
                    continue;
            }

            if (bracketCount != 0) continue;

            switch (priority) {
                case MIN_PRIORITY:
                    switch (type) {
                        case ADD:
                            split(start, i, end);
                            addCommand(stream, VirtualMachine.ADD);
                            return;
                        case SUB:
                            split(start, i, end);
                            addCommand(stream, VirtualMachine.SUB);
                            return;
                        default:
                            continue;
                    }
                case MIN_PRIORITY + 1:
                    switch (type) {
                        case MUL:
                            split(start, i, end);
                            addCommand(stream, VirtualMachine.MUL);
                            return;
                        case DIV:
                            split(start, i, end);
                            addCommand(stream, VirtualMachine.DIV);
                            return;
                        default:
                            continue;
                    }
                case MIN_PRIORITY + 2:
                    throw new RuntimeException("Unprocessed operator: " + type.value);
                default:
                    throw new RuntimeException("Invalid priority: " + priority);
            }
        }

        recursion(start, end, priority + 1);
    }

    private static void split(int start, int i, int end) {
        recursion(i + 1, end, MIN_PRIORITY);
        recursion(start, i - 1, MIN_PRIORITY);
    }

    private static boolean haveOutsideOperator(int start, int end) {
        int bracketCount = 0;
        for (int i = start; i <= end; i++) {
            switch (list.get(i).getType()) {
                case NUMBER:
                    continue;
                case OPEN:
                    bracketCount++;
                    continue;
                case CLOSE:
                    bracketCount--;
                    continue;
                case ADD:
                case SUB:
                case MUL:
                case DIV:
                    if (bracketCount == 0) {
                        return true;
                    }
            }
        }

        return false;
    }
}
