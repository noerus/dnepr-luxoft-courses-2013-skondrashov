package com.luxoft.dnepr.courses.compiler.util;

public enum TokenType {
    MUL("*", 3),
    DIV("/", 3),
    ADD("+", 2),
    SUB("-", 2),
    CLOSE(")", 1),
    OPEN("(", 0),
    NUMBER(null, -1);

    public final String value;
    public final int priority;

    TokenType(String value, int priority) {
        this.value = value;
        this.priority = priority;
    }
}
