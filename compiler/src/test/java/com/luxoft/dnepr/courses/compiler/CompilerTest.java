package com.luxoft.dnepr.courses.compiler;

import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
        assertCompiled(5.99, " 2.55 + 3.44 ");
    }

    @Test
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");

        assertCompiled(4, "(2 + 2)");
        assertCompiled(4, "((2 + 2))");
        assertCompiled(6, "2 + 2 * 2");
        assertCompiled(8, "(2 + 2) * 2");
        assertCompiled(((((2.) / 3) / 5) / 7), "((((2)/3)/5)/7)");
        assertCompiled((2 / (3 / (5 / (7.)))), "(2/(3/(5/(7))))");
        assertCompiled(1.0 / 2 / 3 / 4, "1/2/3/4");
        assertCompiled(2. * 2 / 2 * 2, "2 * 2 / 2 * 2");
        assertCompiled(29, "5 + 2 * (14 - 3 * (8 - 6)) + 32 / (10 - 2*3)");
        assertCompiled(2, "2");
        assertCompiled(2, "(2)");

        assertCompiled(1, " (2+2)/(2+2)");
        assertCompiled(1, "  100 / 2 / 5 / 10");
        assertCompiled(12, "(((2 + 2 )) * 3)");

    }

    @Test(expected = CompilationException.class)
    public void testException1() {
        assertCompiled(0, "");
    }

    @Test(expected = CompilationException.class)
    public void testException2() {
        assertCompiled(0, "2 + + 2");
    }

    @Test(expected = CompilationException.class)
    public void testException3() {
        assertCompiled(0, "2 + 2 ) * ( 2 + 2");
    }

    @Test(expected = CompilationException.class)
    public void testException4() {
        assertCompiled(0, "2 +");
    }

    @Test(expected = CompilationException.class)
    public void testException5() {
        assertCompiled(0, "+");
    }

    @Test(expected = CompilationException.class)
    public void testException6() {
        assertCompiled(0, "2 + 2..2 + 2");
    }

    @Test(expected = CompilationException.class)
    public void testException7() {
        assertCompiled(0, "2.2. + 2");
    }

    @Test(expected = CompilationException.class)
    public void testException8() {
        assertCompiled(0, "a");
    }

    @Test(expected = CompilationException.class)
    public void testException9() {
        assertCompiled(0, "2+2)");
    }

    @Test(expected = CompilationException.class)
    public void testException10() {
        assertCompiled(0, "()");
    }

    @Test(expected = CompilationException.class)
    public void testException11() {
        assertCompiled(0, "2 + 2 2 + 2");
    }

}
