package com.luxoft.dnepr.courses.unit11;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public class Dummy extends HttpServlet {
    private static ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setContentTypeAndEncoding(resp);

        String name = req.getParameter("name");
        String age = req.getParameter("age");
        if (nameOrAgeIsNull(name, age, resp)) return;

        if (map.putIfAbsent(name, age) == null) {
            resp.setStatus(HttpServletResponse.SC_CREATED);
        } else {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write(new Gson().toJson(Error.nameAlreadyExists(name)));
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setContentTypeAndEncoding(resp);

        String name = req.getParameter("name");
        String age = req.getParameter("age");
        if (nameOrAgeIsNull(name, age, resp)) return;

        if (map.replace(name, age) != null) {
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
        } else {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write(new Gson().toJson(Error.nameDoesNotExist(name)));
        }
    }

    private void setContentTypeAndEncoding(HttpServletResponse resp) {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");
    }

    private boolean nameOrAgeIsNull(String name, String age, HttpServletResponse resp) throws IOException {
        if (name == null || age == null) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write(new Gson().toJson(Error.illegalParameters()));
            return true;
        }
        return false;
    }

    private static class Error {
        private static final String ILLEGAL_PARAMETERS = "Illegal parameters";
        private static final String ALREADY_EXISTS = "Name ${name} already exists";
        private static final String DOESNOT_EXIST = "Name ${name} does not exist";

        private String error;

        private Error(String error) {
            this.error = error;
        }

        public static Error illegalParameters() {
            return new Error(ILLEGAL_PARAMETERS);
        }

        public static Error nameAlreadyExists(String name) {
            return new Error(ALREADY_EXISTS.replace("${name}", name));
        }

        public static Error nameDoesNotExist(String name) {
            return new Error(DOESNOT_EXIST.replace("${name}", name));
        }
    }
}
