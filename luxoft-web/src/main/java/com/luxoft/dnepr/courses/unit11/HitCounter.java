package com.luxoft.dnepr.courses.unit11;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class HitCounter extends HttpServlet {
    private static AtomicInteger hitCount = new AtomicInteger(0);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");

        resp.getWriter().write("{\"hitCount\": " + hitCount.incrementAndGet() + "}");
    }
}
