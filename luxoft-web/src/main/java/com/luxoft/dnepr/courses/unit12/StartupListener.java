package com.luxoft.dnepr.courses.unit12;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StartupListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
        Map<String, String> map = new ConcurrentHashMap<>();
        sc.setAttribute("users", map);

        InputStream xmlFile = sc.getResourceAsStream("/META-INF/" + sc.getInitParameter("users"));
        Document doc;
        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        NodeList users = doc.getDocumentElement().getElementsByTagName("user");
        for (int i = 0; i < users.getLength(); i++) {
            String name = ((Element) users.item(i)).getAttribute("name");
            String password = ((Element) users.item(i)).getAttribute("password");
            map.put(name, password);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
