package com.luxoft.dnepr.courses.unit12;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class UserServlet extends HttpServlet {
    private static final Cookie LOG_IN = new Cookie("error", "Please, log in!");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);

        if (session == null) {
            resp.addCookie(LOG_IN);
            resp.sendRedirect("");
        } else {
            resp.getWriter().print("<html><body><center>");
            resp.getWriter().print("<a href=\"logout\">logout</a>");
            resp.getWriter().print("<h1>Hello " + session.getAttribute("login") + "!</h1>");
            resp.getWriter().print("</center></body></html>");
        }
    }
}
