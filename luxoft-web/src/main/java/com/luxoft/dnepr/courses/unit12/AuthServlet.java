package com.luxoft.dnepr.courses.unit12;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

public class AuthServlet extends HttpServlet {
    private static final Cookie WRONG_LOGIN_OR_PASS = new Cookie("error", "Wrong login or password");

    //login
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!req.getServletPath().equals("/login")) return;

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        Map<String, String> users = (Map) getServletContext().getAttribute("users");

        if (users.containsKey(login) && Objects.equals(users.get(login), password)) {
            HttpSession session = req.getSession(true);
            synchronized (session) {
                session.setAttribute("login", login);
            }
            resp.sendRedirect("user");
        } else {
            resp.addCookie(WRONG_LOGIN_OR_PASS);
            resp.sendRedirect("");
        }
    }

    //logout
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!req.getServletPath().equals("/logout")) return;

        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        Cookie remove = new Cookie("JSESSIONID", "");
        remove.setMaxAge(0);
        remove.setPath(req.getContextPath() + "/");
        resp.addCookie(remove);

        resp.sendRedirect("");
    }
}
