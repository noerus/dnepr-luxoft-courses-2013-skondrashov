package com.luxoft.dnepr.courses.regular.unit3;

public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;

    public User() {
    }

    public User(Long id, String name, WalletInterface wallet) {
        this.id = id;
        this.name = name;
        this.wallet = wallet;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}
