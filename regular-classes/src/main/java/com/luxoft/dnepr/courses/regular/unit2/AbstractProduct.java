package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Objects;

public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;

        AbstractProduct product = (AbstractProduct) obj;
        return Objects.equals(code, product.code) && Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        int hash = 13;
        hash = 103 * hash + (code == null ? 0 : code.hashCode());
        hash = 103 * hash + (name == null ? 0 : name.hashCode());
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
