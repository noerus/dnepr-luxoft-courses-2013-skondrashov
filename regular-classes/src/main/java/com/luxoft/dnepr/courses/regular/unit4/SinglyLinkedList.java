package com.luxoft.dnepr.courses.regular.unit4;

import java.util.Iterator;
import java.util.Objects;

public class SinglyLinkedList<T> implements Iterable<T> {
    private Node first = new Node();
    private Node last = first;

    private int size = 0;

    public void add(T element) {
        last.value = element;
        last.next = new Node();
        last = last.next;
        size++;
    }

    public boolean remove(Object obj) {
        if (first == last) return false;

        if (Objects.equals(first.value, obj)) {
            first = first.next;
            size--;
            return true;
        } else {
            Node prev = first;
            for (Node curr = first.next; curr.next != null; prev = curr, curr = curr.next) {
                if (Objects.equals(curr.value, obj)) {
                    prev.next = curr.next;
                    size--;
                    return true;
                }
            }
        }

        return false;
    }

    public void clear() {
        first = last;
        size = 0;
    }

    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iter();
    }

    private void removeNode(Node node) {
        if (first == last) return;

        if (node == null) throw new IllegalStateException();

        if (node == first) {
            first = first.next;
        } else {
            Node p = first;
            while (p.next != node) p = p.next;
            p.next = node.next;
        }
        size--;
    }

    private class Node {
        public T value;
        public Node next;
    }


    private class Iter implements Iterator<T> {
        private Node cursor = first;
        private Node prev;

        @Override
        public boolean hasNext() {
            return cursor.next != null;
        }

        @Override
        public T next() {
            prev = cursor;
            cursor = cursor.next;
            return prev.value;
        }

        @Override
        public void remove() {
            removeNode(prev);
        }
    }

}
