package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {
    private double side;

    public Hexagon(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return (3. / 2.) * Math.sqrt(3.) * Math.pow(side, 2);
    }
}
