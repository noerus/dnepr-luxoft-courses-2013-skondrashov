package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Represents general contract of Product.
 */
public interface Product {
    String getCode();

    String getName();

    double getPrice();
}
