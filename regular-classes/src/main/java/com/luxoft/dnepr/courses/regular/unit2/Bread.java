package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        if (!super.equals(obj)) return false;

        Bread bread = (Bread) obj;
        return Double.compare(weight, bread.weight) == 0;
    }

    @Override
    public int hashCode() {
        return super.hashCode() * 103 + new Double(weight).hashCode();
    }
}
