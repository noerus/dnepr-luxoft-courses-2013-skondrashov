package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<CompositeProduct> bill = new LinkedList<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        for (CompositeProduct compositeProduct : bill) {
            if (compositeProduct.getChild().equals(product)) {
                compositeProduct.add(product);
                return;
            }
        }

        CompositeProduct compositeProduct = new CompositeProduct();
        compositeProduct.add(product);
        bill.add(0, compositeProduct);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double sum = 0;
        for (Product product : bill) {
            sum += product.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        Collections.sort(bill, new ProductComparator());
        return new ArrayList<Product>(bill);
    }

    @Override
    public String toString() {
        StringBuilder productInfos = new StringBuilder();
        for (Product product : getProducts()) {
            productInfos.append(product.toString());
            productInfos.append("\n");
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    private class ProductComparator implements Comparator<Product> {
        @Override
        public int compare(Product o1, Product o2) {
            return Double.compare(o2.getPrice(), o1.getPrice());
        }
    }
}
