package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Map;

public class Bank implements BankInterface {
    private static final String USER_BLOCKED = "User '${user.name}' wallet is blocked";
    private static final String USER_INSUFF = "User '${user.name}' has insufficient funds (${user.wallet.amount} < ${amountToWithdraw})";
    private static final String USER_LIMIT = "User '${user.name}' wallet limit exceeded (${user.wallet.amount} + ${amountToWithdraw} > ${user.wallet.maxAmount})";

    private static final String JAVA_VERSION_KEY = "java.version";

    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) {
        if (expectedJavaVersion == null) throw new IllegalArgumentException("expectedJavaVersion is null");

        String actualJavaVersion = System.getProperty(JAVA_VERSION_KEY);
        if (actualJavaVersion == null) {
            throw new IllegalArgumentException("Unknown system property: " + JAVA_VERSION_KEY);
        }

        expectedJavaVersion = expectedJavaVersion.trim();
        if (!actualJavaVersion.startsWith(expectedJavaVersion) || expectedJavaVersion.isEmpty()) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion,
                    "Actual: " + actualJavaVersion + ", expected: " + expectedJavaVersion);
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        if (fromUserId == null) throw new IllegalArgumentException("fromUserId is null");
        if (toUserId == null) throw new IllegalArgumentException("toUserId is null");
        if (amount == null) throw new IllegalArgumentException("amount is null");

        UserInterface userFrom = findUser(fromUserId);
        UserInterface userTo = findUser(toUserId);

        checkPossibilityOfTransaction(userFrom, userTo, amount);

        userFrom.getWallet().withdraw(amount);
        userTo.getWallet().transfer(amount);
    }

    private UserInterface findUser(Long id) throws NoUserFoundException {
        if (users == null) throw new IllegalStateException("users is null");

        UserInterface user = users.get(id);
        if (user == null) {
            throw new NoUserFoundException(id, "User ID: " + id);
        }

        if (user.getWallet() == null) throw new IllegalStateException("user.wallet is null");
        if (user.getName() == null) throw new IllegalStateException("user.name is null");
        if (user.getId() == null) throw new IllegalStateException("user.id is null");
        return user;
    }

    private void checkPossibilityOfTransaction(UserInterface userFrom, UserInterface userTo, BigDecimal amount) throws TransactionException {
        UserInterface user = userFrom;
        try {
            user.getWallet().checkWithdrawal(amount);
            user = userTo;
            user.getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException(getMessageBlocked(user));
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException(getMessageInsufficient(user, amount));
        } catch (LimitExceededException e) {
            throw new TransactionException(getMessageLimit(user, amount));
        }
    }

    private String getMessageBlocked(UserInterface user) {
        return USER_BLOCKED
                .replace("${user.name}", user.getName());
    }

    private String getMessageInsufficient(UserInterface user, BigDecimal amount) {
        return USER_INSUFF
                .replace("${user.name}", user.getName())
                .replace("${user.wallet.amount}", formatBigDec(user.getWallet().getAmount()))
                .replace("${amountToWithdraw}", formatBigDec(amount));
    }

    private String getMessageLimit(UserInterface user, BigDecimal amount) {
        return USER_LIMIT
                .replace("${user.name}", user.getName())
                .replace("${user.wallet.amount}", formatBigDec(user.getWallet().getAmount()))
                .replace("${amountToWithdraw}", formatBigDec(amount))
                .replace("${user.wallet.maxAmount}", formatBigDec(user.getWallet().getMaxAmount()));
    }

    private String formatBigDec(BigDecimal number) {
        return number.setScale(2, BigDecimal.ROUND_DOWN).toPlainString();
    }
}
