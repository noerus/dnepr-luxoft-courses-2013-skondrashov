package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {
    private boolean nonAlcoholic;

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        if (!super.equals(obj)) return false;

        Beverage beverage = (Beverage) obj;
        return nonAlcoholic == beverage.nonAlcoholic;
    }

    @Override
    public int hashCode() {
        return super.hashCode() * 103 + (nonAlcoholic ? 1 : 0);
    }
}
