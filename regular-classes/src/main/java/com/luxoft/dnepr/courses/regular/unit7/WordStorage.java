package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represents word statistics storage.
 */
public class WordStorage {
    private Map<String, AtomicInteger> storage = new HashMap<>();

    /**
     * Saves given word and increments count of occurrences.
     *
     * @param word
     */
    public void save(String word) {
        AtomicInteger count;
        synchronized (storage) {
            if ((count = storage.get(word)) != null) {
                count.incrementAndGet();
            } else {
                storage.put(word, new AtomicInteger(1));
            }
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(storage);
    }
}
