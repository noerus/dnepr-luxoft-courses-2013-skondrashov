package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {
    private WordStorage wordStorage;

    private String rootFolder;
    private int maxNumberOfThreads;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        wordStorage = new WordStorage();
        BlockingQueue<File> queue = new ArrayBlockingQueue<>(10);
        List<File> processedFiles = new Vector<>();

        List<Thread> threads = new ArrayList<>();

        threads.add(new Thread(new Producer(queue, rootFolder)));
        for (int i = 1; i < maxNumberOfThreads; i++) {
            threads.add(new Thread(new Consumer(queue, wordStorage, processedFiles)));
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }
}
