package com.luxoft.dnepr.courses.unit1;

import java.util.Arrays;

public final class LuxoftUtils {

    private LuxoftUtils() {}

    public static String getMonthName(int monthOrder, String language) {
        switch (language) {
            case "ru":
                return getMonthNameRu(monthOrder);
            case "en":
                return getMonthNameEn(monthOrder);
            default:
                return "Unknown Language";
        }
    }

    private static String getMonthNameEn(int monthOrder) {
        switch (monthOrder) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "Unknown Month";
        }
    }

    private static String getMonthNameRu(int monthOrder) {
        switch (monthOrder) {
            case 1:
                return "Январь";
            case 2:
                return "Февраль";
            case 3:
                return "Март";
            case 4:
                return "Апрель";
            case 5:
                return "Май";
            case 6:
                return "Июнь";
            case 7:
                return "Июль";
            case 8:
                return "Август";
            case 9:
                return "Сентябрь";
            case 10:
                return "Октябрь";
            case 11:
                return "Ноябрь";
            case 12:
                return "Декабрь";
            default:
                return "Неизвестный месяц";
        }
    }

    public static String binaryToDecimal(String binaryNumber) {
        long res = 0;
        long power = 1;

        for (int i = binaryNumber.length() - 1; i >= 0; i--) {
            char c = binaryNumber.charAt(i);

            switch (c) {
                case '0':
                    break;
                case '1':
                    res += power;
                    break;
                default:
                    return "Not binary";
            }

            power <<= 1;
        }

        return String.valueOf(res);
    }

    public static String decimalToBinary(String decimalNumber) {
        long number = 0;
        long power = 1;

        for (int i = decimalNumber.length() - 1; i >= 0; i--) {
            int cipher = decimalNumber.charAt(i) - '0';

            if (cipher < 0 || cipher > 9) return "Not decimal";

            number += cipher * power;
            power *= 10;
        }

        StringBuilder sb = new StringBuilder(64);

        do {
            if (number % 2 == 0) {
                sb.append('0');
            }
            if (number % 2 == 1) {
                sb.append('1');
            }
            number /= 2;
        } while (number > 0);

        return sb.reverse().toString();
    }

    public static int[] sortArray(int[] array, boolean asc) {
        int[] res = Arrays.copyOf(array, array.length);

        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < res.length - 1; i++) {
                if (asc && (res[i] > res[i + 1]) || !asc && (res[i] < res[i + 1])) {
                    int t = res[i + 1];
                    res[i + 1] = res[i];
                    res[i] = t;

                    sorted = false;
                }
            }
        }

        return res;
    }
}
