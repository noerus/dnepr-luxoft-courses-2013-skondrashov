package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.IOException;
import java.io.Reader;

public final class JsonUtils {
    private static final String INDENT = "\t";
    private static final String EOL = "\n";

    private JsonUtils() {
    }

    public static FamilyTree jsonToFamilyTree(Reader input) throws IOException {
        FamilyTree result = FamilyTreeImpl.create(null);
        int c = cutSpaces(input);
        if (c != '{') errorUnexpChar(c, input);

        String name;
        while (c != '}' && (name = readStringValue(input)) != null) {
            if (cutSpaces(input) != ':') errorUnexpChar(c, input);
            switch (name) {
                case "root":
                    result = FamilyTreeImpl.create(jsonToPerson(input));
                    break;
                default:
                    throw new IOException("Unknown name: " + name);
            }
            c = cutSpaces(input);
            if (!(c == ',' || c == '}')) errorUnexpChar(c, input);
        }

        return result;
    }

    public static Person jsonToPerson(Reader input) throws IOException {
        PersonImpl result = new PersonImpl();
        int c = cutSpaces(input);
        if (c != '{') errorUnexpChar(c, input);

        String name;
        while (c != '}' && (name = readStringValue(input)) != null) {
            if (cutSpaces(input) != ':') errorUnexpChar(c, input);
            switch (name) {
                case "father":
                    result.setFather(jsonToPerson(input));
                    break;
                case "mother":
                    result.setMother(jsonToPerson(input));
                    break;
                case "name":
                    result.setName(readStringValue(input));
                    break;
                case "ethnicity":
                    result.setEthnicity(readStringValue(input));
                    break;
                case "age":
                    result.setAge(Integer.parseInt(readValue(input)));
                    break;
                case "gender":
                    result.setGender(Gender.valueOf(readValue(input)));
                    break;
                default:
                    throw new IOException("Unknown name: " + name);
            }
            c = cutSpaces(input);
            if (!(c == ',' || c == '}')) errorUnexpChar(c, input);
        }

        return result;
    }

    private static String readValue(Reader input) throws IOException {
        StringBuilder result = new StringBuilder();

        int c = cutSpaces(input);
        while (!(c == ',' || c == '}' || c == -1)) {
            result.append((char) c);
            input.mark(10);
            c = input.read();
        }
        input.reset();

        return result.toString().trim();
    }

    private static String readStringValue(Reader input) throws IOException {
        StringBuilder result = new StringBuilder();
        boolean inString = false;

        int c = cutSpaces(input);
        switch (c) {
            case '}':
                return null;
            case '"':
                inString = true;
                break;
            default:
                errorUnexpChar(c, input);
        }

        while (inString) {
            c = input.read();
            switch (c) {
                case -1:
                    return null;
                case '\\':
                    result.append((char) input.read());
                    break;
                case '"':
                    inString = false;
                    break;
                default:
                    result.append((char) c);
            }
        }

        return result.toString();
    }

    private static int cutSpaces(Reader input) throws IOException {
        int c;
        do {
            c = input.read();
        } while (Character.isWhitespace(c));
        return c;
    }

    private static void errorUnexpChar(int c, Reader input) throws IOException {
        if (c == -1) throw new IOException("Unexpected end of sequence");
        char[] buf = new char[1000];
        input.read(buf);
        throw new IOException(String.format("Unexpected char (%d): %c%s", c, c, String.valueOf(buf)));
    }

    public static String familyTreeToJson(FamilyTree familyTree) {
        StringBuilder result = new StringBuilder();

        result.append('{' + EOL);
        if (familyTree.getRoot() != null) {
            result.append(formatName("root", 1));
            result.append(personToJson(familyTree.getRoot(), 2));
        }

        // deleting last comma
        int position = result.length() - EOL.length() - 1;
        if (result.charAt(position) == ',') {
            result.deleteCharAt(position);
        }

        result.append("}" + EOL);
        return result.toString();
    }

    public static String personToJson(Person person, int depth) {
        StringBuilder result = new StringBuilder();

        result.append('{' + EOL);
        if (person.getName() != null) {
            result.append(formatName("name", depth));
            result.append(formatValue(person.getName()));
        }
        if (person.getGender() != null) {
            result.append(formatName("gender", depth));
            result.append(formatValue(person.getGender()));
        }
        if (person.getAge() != 0) {
            result.append(formatName("age", depth));
            result.append(formatValue(person.getAge()));
        }
        if (person.getEthnicity() != null) {
            result.append(formatName("ethnicity", depth));
            result.append(formatValue(person.getEthnicity()));
        }
        if (person.getFather() != null) {
            result.append(formatName("father", depth));
            result.append(personToJson(person.getFather(), depth + 1));
        }
        if (person.getMother() != null) {
            result.append(formatName("mother", depth));
            result.append(personToJson(person.getMother(), depth + 1));
        }

        // deleting last comma
        int position = result.length() - EOL.length() - 1;
        if (result.charAt(position) == ',') {
            result.deleteCharAt(position);
        }

        for (int i = 0; i < depth - 1; i++) {
            result.append(INDENT);
        }
        result.append("}," + EOL);

        return result.toString();
    }

    private static String formatValue(int value) {
        return String.valueOf(value) + ',' + EOL;
    }

    private static String formatValue(Gender value) {
        return value.toString() + ',' + EOL;
    }

    private static String formatValue(String value) {
        return '"' + value
                .replace("\\", "\\\\")
                .replace("\"", "\\\"")
                + "\"," + EOL;
    }

    private static String formatName(String name, int depth) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < depth; i++) {
            result.append(INDENT);
        }
        result.append('"' + name + '"' + ": ");

        return result.toString();
    }
}
