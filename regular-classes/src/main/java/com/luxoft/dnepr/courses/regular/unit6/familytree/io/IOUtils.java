package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public final class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(InputStream is) {
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            return (FamilyTree) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static FamilyTree load(String filename) {
        try (FileInputStream fis = new FileInputStream(filename)) {
            return load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void save(OutputStream os, FamilyTree familyTree) {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(familyTree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void save(String filename, FamilyTree familyTree) {
        try (FileOutputStream fos = new FileOutputStream(filename)) {
            save(fos, familyTree);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
