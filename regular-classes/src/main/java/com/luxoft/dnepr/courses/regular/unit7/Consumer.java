package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private BlockingQueue<File> queue;
    private WordStorage wordStorage;
    private List<File> processedFiles;

    public Consumer(BlockingQueue<File> queue, WordStorage wordStorage, List<File> processedFiles) {
        this.queue = queue;
        this.wordStorage = wordStorage;
        this.processedFiles = processedFiles;
    }

    @Override
    public void run() {
        try {
            while (true) {
                File txtFile = queue.take();
                if (txtFile.getName().isEmpty()) {
                    queue.put(txtFile);
                    return;
                } else {
                    process(txtFile);
                    processedFiles.add(txtFile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void process(File txtFile) throws IOException {
        Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(txtFile), "UTF-8"));
        StringBuilder fileContent = new StringBuilder();

        char[] buf = new char[1024];
        int r;

        while ((r = reader.read(buf)) != -1) {
            fileContent.append(buf, 0, r);
        }

        String[] wordlist = fileContent.toString().split("[^A-Za-zА-Яа-я0-9Ёё]+");
        for (String word : wordlist) {
            if (!word.isEmpty()) {
                wordStorage.save(word);
            }
        }
    }
}
