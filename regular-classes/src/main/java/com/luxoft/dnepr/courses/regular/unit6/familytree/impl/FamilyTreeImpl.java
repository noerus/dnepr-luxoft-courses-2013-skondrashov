package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.JsonUtils;

import java.io.*;

public class FamilyTreeImpl implements FamilyTree {
	
	private static final long serialVersionUID = 3057396458981676327L;
	private Person root;
	private transient long creationTime;
	
	private FamilyTreeImpl(Person root, long creationTime) {
		this.root = root;
		this.creationTime = creationTime;
	}
	
	public static FamilyTree create(Person root) {
		return new FamilyTreeImpl(root, System.currentTimeMillis());
	}
	
	@Override
	public Person getRoot() {
		return root;
	}
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}

    private void writeObject(ObjectOutputStream oos) throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(oos);
        osw.write(JsonUtils.familyTreeToJson(this));
        osw.flush();
    }

    private void readObject(ObjectInputStream ois) throws IOException {
        Reader reader = new BufferedReader(new InputStreamReader(ois));
        this.root = JsonUtils.jsonToFamilyTree(reader).getRoot();
    }
}
