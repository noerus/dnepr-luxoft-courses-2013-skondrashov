package com.luxoft.dnepr.courses.regular.unit4;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public class EqualSet<E> implements Set<E> {
    private SinglyLinkedList<E> elements = new SinglyLinkedList<>();

    public EqualSet() {
    }

    public EqualSet(Collection<? extends E> collection) {
        addAll(collection);
    }

    @Override
    public boolean add(E e) {
        if (contains(e)) {
            return false;
        }
        elements.add(e);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (E element : elements) {
            if (Objects.equals(element, o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return elements.iterator();
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public Object[] toArray() {
        Object[] res = new Object[elements.size()];
        int i = 0;
        for (E element : elements) {
            res[i++] = element;
        }
        return res;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < elements.size()) {
            a = (T[]) java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), elements.size());
        }

        int i = 0;
        for (E element : elements) {
            a[i++] = (T) element;
        }

        if (a.length > elements.size()) {
            a[elements.size()] = null;
        }

        return a;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object obj : c) {
            if (!contains(obj)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean changed = false;
        for (E elem : c) {
            if (add(elem)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object obj : c) {
            if (remove(obj)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        for (E element : elements) {
            if (!c.contains(element)) {
                remove(element);
                changed = true;
            }
        }
        return changed;
    }
}
