package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet() {
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (amountToWithdraw == null) throw new IllegalArgumentException("amountToWithdraw is null");
        checkState();
        checkBlocked();
        if (amountToWithdraw.compareTo(amount) > 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "Wallet ID: " + id);
        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        if (amountToWithdraw == null) throw new IllegalArgumentException("amountToWithdraw is null");
        checkState();
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (amountToTransfer == null) throw new IllegalArgumentException("amountToTransfer is null");
        checkState();
        checkBlocked();
        if (amountToTransfer.add(amount).compareTo(maxAmount) > 0) {
            throw new LimitExceededException(id, amountToTransfer, amount, "Wallet ID: " + id);
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        if (amountToTransfer == null) throw new IllegalArgumentException("amountToTransfer is null");
        checkState();
        amount = amount.add(amountToTransfer);
    }

    private void checkBlocked() throws WalletIsBlockedException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, "Wallet ID: " + id);
        }
    }

    private void checkState() {
        if (id == null) throw new IllegalStateException("id is null");
        if (amount == null) throw new IllegalStateException("amount is null");
        if (status == null) throw new IllegalStateException("status is null");
        if (maxAmount == null) throw new IllegalStateException("maxAmount is null");
    }
}
