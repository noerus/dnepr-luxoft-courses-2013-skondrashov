package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

public class EntityStorage<T extends Entity> {
    private final Map<Long, T> entities = new HashMap();

    public Map<Long, T> getEntities() {
        return entities;
    }

    public Long getMaxId() {
        Long max = Long.valueOf(0);
        for (Long key : entities.keySet()) {
            if (key > max) {
                max = key;
            }
        }
        return max;
    }
}
