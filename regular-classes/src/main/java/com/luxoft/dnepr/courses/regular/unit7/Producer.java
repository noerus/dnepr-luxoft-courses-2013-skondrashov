package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    private BlockingQueue<File> queue;
    private String rootFolder;

    public Producer(BlockingQueue<File> queue, String rootFolder) {
        this.queue = queue;
        this.rootFolder = rootFolder;
    }

    @Override
    public void run() {
        try {
            searchTxtFiles(new File(rootFolder));
            queue.put(new File(""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchTxtFiles(File dir) throws InterruptedException {
        File[] files = dir.listFiles();
        if (files == null) {
            return;
        }

        for (File file : files) {
            if (file.isDirectory()) {
                searchTxtFiles(file);
            } else {
                if (file.getName().endsWith(".txt")) {
                    queue.put(file);
                }
            }
        }
    }
}
