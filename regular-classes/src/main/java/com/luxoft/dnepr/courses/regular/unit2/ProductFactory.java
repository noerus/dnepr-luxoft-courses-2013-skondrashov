package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Calendar;
import java.util.Date;

/**
 * Product factory.
 * Simplifies creation of different kinds of products.
 */
public class ProductFactory {

    public Bread createBread(String code, String name, double price, double weight) {
        Bread bread = new Bread();
        bread.setCode(code);
        bread.setName(name);
        bread.setPrice(price);
        bread.setWeight(weight);
        return bread;
    }

    public Beverage createBeverage(String code, String name, double price, boolean nonAlcoholic) {
        Beverage beverage = new Beverage();
        beverage.setCode(code);
        beverage.setName(name);
        beverage.setPrice(price);
        beverage.setNonAlcoholic(nonAlcoholic);
        return beverage;
    }

    public Book createBook(String code, String name, double price, Date publicationDate) {
        Book book = new Book();
        book.setCode(code);
        book.setName(name);
        book.setPrice(price);
        book.setPublicationDate(publicationDate);
        return book;
    }
}
