package com.luxoft.dnepr.courses.unit2.model;

public abstract class Figure {
    public abstract double calculateArea();
}