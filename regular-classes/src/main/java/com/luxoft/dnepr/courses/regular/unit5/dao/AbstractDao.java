package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public class AbstractDao<T extends Entity> implements IDao<T> {
    private EntityStorage<T> storage = new EntityStorage<>();

    public EntityStorage<T> getStorage() {
        return storage;
    }

    @Override
    public T save(T entity) {
        if (entity == null) throw new IllegalArgumentException("entity is null");
        Long id = entity.getId();

        if (id == null) {
            entity.setId(storage.getMaxId() + 1);
        } else if (storage.getEntities().containsKey(id)) {
            throw new UserAlreadyExist();
        }
        storage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public T update(T entity) {
        if (entity == null) throw new IllegalArgumentException("entity is null");
        Long id = entity.getId();

        if (id == null || !storage.getEntities().containsKey(id)) {
            throw new UserNotFound();
        }
        storage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public T get(long id) {
        return storage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        return storage.getEntities().remove(id) != null;
    }
}
