package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    public static String[] sortArray(String[] array, boolean asc) {
        String[] res = Arrays.copyOf(array, array.length);

        Arrays.sort(res); //mergesort (ascending)

        if (!asc) { //reverse asc to desc
            int len = res.length;
            for (int i = 0; i < len / 2; i++) {
                String s = res[i];
                res[i] = res[len - 1 - i];
                res[len - 1 - i] = s;
            }
        }

        return res;
    }

    public static double wordAverageLength(String str) {
        String[] split = str.split("\\s+");

        int count = 0;
        for (String s : split) {
            count += s.length();
        }

        return (double) count / split.length;
    }

    public static String reverseWords(String str) {
        String[] split = str.split("((?<=\\s)|(?=\\s))"); //split with save spaces

        StringBuilder res = new StringBuilder(str.length());

        for (String s : split) {
            for (int i = s.length() - 1; i >= 0; i--) {
                res.append(s.charAt(i));
            }
        }

        return res.toString();
    }

    public static char[] getCharEntries(String str) {
        final Map<Character, Integer> map = new TreeMap<>();

        for (char c : str.toCharArray()) {
            if (!Character.isLetter(c)) continue;

            if (!map.containsKey(c)) {
                map.put(c, 1);
                continue;
            }

            map.put(c, map.get(c) + 1);
        }

        List<Character> list = new ArrayList<>(map.keySet());

        Collections.sort(list, new Comparator<Character>() {
            @Override
            public int compare(Character o1, Character o2) {
                if (map.get(o1) < map.get(o2)) return 1;
                if (map.get(o1) > map.get(o2)) return -1;
                return 0;
            }
        });

        char[] res = new char[list.size()];

        for (int i = 0; i < res.length; i++) {
            res[i] = list.get(i);
        }

        return res;
    }

    public static double calculateOverallArea(List<Figure> figures) {
        double sum = 0;
        for (Figure f : figures) {
            sum += f.calculateArea();
        }

        return sum;
    }
}
