package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;
import java.util.Objects;

public class Book extends AbstractProduct {
    private Date publicationDate;

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        if (!super.equals(obj)) return false;

        Book book = (Book) obj;
        return Objects.equals(publicationDate, book.publicationDate);
    }

    @Override
    public int hashCode() {
        return super.hashCode() * 103 + publicationDate.hashCode();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book book = (Book) super.clone();
        if (book.publicationDate != null) {
            book.publicationDate = (Date) book.publicationDate.clone();
        }
        return book;
    }
}
