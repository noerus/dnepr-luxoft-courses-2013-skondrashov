package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class WalletTest {
    @Test
    public void checkWithdrawalTest() {
        Wallet wallet = new Wallet(0L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO);

        try {
            wallet.checkWithdrawal(BigDecimal.ZERO);
            fail();
        } catch (Exception e) {
            if (e.getClass() != WalletIsBlockedException.class) fail();
        }

        wallet.setStatus(WalletStatus.ACTIVE);
        try {
            wallet.checkWithdrawal(BigDecimal.ZERO);
        } catch (Exception e) {
            fail();
        }

        try {
            wallet.checkWithdrawal(BigDecimal.ONE);
            fail();
        } catch (Exception e) {
            if (e.getClass() != InsufficientWalletAmountException.class) fail();
        }
    }

    @Test
    public void withdrawTest() {
        Wallet wallet = new Wallet(0L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO);
        wallet.setAmount(BigDecimal.valueOf(100));
        wallet.withdraw(BigDecimal.valueOf(33));
        assertEquals(BigDecimal.valueOf(67), wallet.getAmount());
    }

    @Test
    public void checkTransferTest() {
        Wallet wallet = new Wallet(0L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO);

        try {
            wallet.checkTransfer(BigDecimal.ZERO);
            fail();
        } catch (Exception e) {
            if (e.getClass() != WalletIsBlockedException.class) fail();
        }

        wallet.setStatus(WalletStatus.ACTIVE);
        try {
            wallet.checkTransfer(BigDecimal.ZERO);
        } catch (Exception e) {
            fail();
        }

        try {
            wallet.checkTransfer(BigDecimal.ONE);
            fail();
        } catch (Exception e) {
            if (e.getClass() != LimitExceededException.class) fail();
        }
    }

    @Test
    public void transferTest() {
        Wallet wallet = new Wallet(0L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO);
        wallet.setAmount(BigDecimal.valueOf(100));
        wallet.transfer(BigDecimal.valueOf(33));
        assertEquals(BigDecimal.valueOf(133), wallet.getAmount());
    }

    @Test
    public void walletTest() {
        Wallet wallet = new Wallet(0L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO);

        assertEquals(Long.valueOf(0), wallet.getId());
        assertEquals(BigDecimal.ZERO, wallet.getAmount());
        assertEquals(WalletStatus.BLOCKED, wallet.getStatus());
        assertEquals(BigDecimal.ZERO, wallet.getMaxAmount());

        wallet.setId(100L);
        wallet.setAmount(BigDecimal.TEN);
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.setMaxAmount(BigDecimal.ONE);

        assertEquals(Long.valueOf(100), wallet.getId());
        assertEquals(BigDecimal.valueOf(10), wallet.getAmount());
        assertEquals(WalletStatus.ACTIVE, wallet.getStatus());
        assertEquals(BigDecimal.valueOf(1), wallet.getMaxAmount());
    }
}
