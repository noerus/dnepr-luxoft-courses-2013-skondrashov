package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class EqualSetTest {
    private Set<Integer> set;

    @Before
    public void setUp() {
        set = new EqualSet<>();
    }

    @Test
    public void addTest() {
        assertEquals(0, set.size());
        assertTrue(set.add(1));
        assertEquals(1, set.size());
        assertFalse(set.add(1));
        assertEquals(1, set.size());
        assertFalse(set.add(1));
        assertEquals(1, set.size());
        assertTrue(set.add(2));
        assertEquals(2, set.size());
        assertFalse(set.add(1));
        assertEquals(2, set.size());
        assertTrue(set.add(null));
        assertEquals(3, set.size());
        assertFalse(set.add(null));
        assertEquals(3, set.size());
    }

    @Test
    public void removeTest() {
        set.add(1);
        set.add(null);
        set.add(2);
        set.add(null);
        set.add(3);
        set.add(3);

        assertEquals(4, set.size());
        assertTrue(set.remove(null));
        assertEquals(3, set.size());
        assertFalse(set.remove(null));
        assertEquals(3, set.size());
        assertTrue(set.remove(1));
        assertTrue(set.remove(2));
        assertFalse(set.remove(2));
        assertTrue(set.remove(3));
        assertFalse(set.remove(3));
        assertEquals(0, set.size());
    }

    @Test
    public void sizeTest() {
        assertEquals(0, set.size());
        int max = 10000;
        for (int i = 0; i <= max; i++) {
            set.add(i);
        }
        assertEquals(max + 1, set.size());
        for (int i = 1; i < max; i++) {
            set.remove(i);
        }
        assertEquals(2, set.size());
        for (int i = 0; i <= max; i++) {
            set.remove(i);
        }
        assertEquals(0, set.size());
    }

    @Test
    public void isEmptyTest() {
        assertTrue(set.isEmpty());
        set.add(1);
        assertFalse(set.isEmpty());
        set.remove(1);
        assertTrue(set.isEmpty());
    }

    @Test
    public void containsTest() {
        assertEquals(0, set.size());
        assertFalse(set.contains(1234));
        set.add(1234);
        assertTrue(set.contains(1234));
        set.remove(1234);
        assertFalse(set.contains(1234));
    }

    @Test
    public void iteratorTest() {
        assertNotNull(set.iterator());

        int[] arr = new int[]{3, 2, 4, 5, 1};
        int[] res = new int[arr.length];
        for (int i : arr) {
            set.add(i);
        }

        Iterator<Integer> iterator = set.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            res[i++] = iterator.next();
        }

        assertArrayEquals(arr, res);
    }

    @Test
    public void iteratorRemovingTest() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        Iterator<Integer> i = set.iterator();
        assertEquals(4, set.size());

        while (i.hasNext()) {
            if (i.next() == 3) i.remove();
        }
        assertEquals(3, set.size());

        i = set.iterator();
        while (i.hasNext()) {
            if (i.next() == 4) i.remove();
        }
        assertEquals(2, set.size());

        i = set.iterator();
        while (i.hasNext()) {
            if (i.next() == 1) i.remove();
        }
        assertEquals(1, set.size());

        i = set.iterator();
        while (i.hasNext()) {
            if (i.next() == 2) i.remove();
        }
        assertEquals(0, set.size());

        set.add(1);
        assertArrayEquals(new Integer[]{1}, set.toArray());
    }

    @Test
    public void clearTest() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        assertEquals(4, set.size());
        assertTrue(set.contains(1));
        assertTrue(set.contains(3));
        set.clear();
        assertEquals(0, set.size());
        assertFalse(set.contains(1));
        assertFalse(set.contains(3));
    }

    @Test
    public void toArrayTest() {
        Collection<Integer> base = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
        for (Integer i : base) {
            set.add(i);
        }
        Collection<Integer> res = new ArrayList<>();
        for (Object o : set.toArray()) {
            assertTrue(base.contains(o));
            res.add((Integer) o);
        }
        base.removeAll(res);
        assertTrue(base.isEmpty());

        Integer[] arr = new Integer[set.size()];
        set.toArray(arr);
        assertEquals(set.size(), res.size());
        res.removeAll(new ArrayList<>(Arrays.asList(arr)));
        assertTrue(res.isEmpty());

        arr = set.toArray(new Integer[2]);
        assertEquals(10, arr.length);
        arr = set.toArray(new Integer[20]);
        assertEquals(20, arr.length);
    }

    @Test
    public void containsAllTest() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);

        Collection<Integer> c = new ArrayList<>(Arrays.asList(3, 5, 4, 1));
        assertTrue(set.containsAll(c));
        c.add(9);
        assertFalse(set.containsAll(c));
    }

    @Test
    public void addAllTest() {
        Collection<Integer> base = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        assertTrue(set.addAll(base));
        assertEquals(4, set.size());
        assertFalse(set.addAll(base));
        assertEquals(4, set.size());
    }

    @Test
    public void removeAllTest() {
        Collection<Integer> base = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        for (Integer i : base) {
            set.add(i);
        }
        assertEquals(4, set.size());
        set.add(5);
        assertTrue(set.removeAll(base));
        assertEquals(1, set.size());
        assertFalse(set.removeAll(base));
        assertEquals(1, set.size());
    }

    @Test
    public void retainAllTest() {
        Collection<Integer> base = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        Collection<Integer> ret = new ArrayList<>(Arrays.asList(1, 3, 8, 4, 5, 9));
        Collection<Integer> sub = new ArrayList<>(Arrays.asList(1, 3, 4, 5));
        set.addAll(base);
        assertEquals(6, set.size());
        assertTrue(set.retainAll(ret));
        assertEquals(4, set.size());
        assertFalse(set.retainAll(ret));
        assertEquals(4, set.size());
        assertTrue(set.removeAll(sub));
        assertEquals(0, set.size());
    }
}
