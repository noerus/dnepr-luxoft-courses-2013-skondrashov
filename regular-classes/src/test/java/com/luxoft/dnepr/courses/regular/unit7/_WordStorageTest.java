package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class _WordStorageTest {
    private WordStorage wordStorage;

    @Before
    public void setUp() {
        wordStorage = new WordStorage();
    }

    @After
    public void tearDown() {
        wordStorage = null;
    }

    @Test
    public void testSave() throws Exception {
        final int numThreads = 50;
        final int numSaves = 100000;
        final String word = "word";

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            threads.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < numSaves; j++) {
                        wordStorage.save(word);
                    }
                }
            }));
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }

        assertEquals(numSaves * numThreads, wordStorage.getWordStatistics().get(word).intValue());
        assertEquals(1, wordStorage.getWordStatistics().size());
    }

    @Test
    public void testSave2() throws Exception {
        final int numThreads = 100;
        final int numSaves = 10000;

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            threads.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < numSaves; j++) {
                        wordStorage.save(Integer.toString(j));
                    }
                }
            }));
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }

        assertEquals(numSaves, wordStorage.getWordStatistics().size());
        for (Number number : wordStorage.getWordStatistics().values()) {
            assertEquals(numThreads, number.intValue());
        }
    }

    @Test
    public void testGetWordStatistics() {
        assertEquals(0, wordStorage.getWordStatistics().size());
        wordStorage.save("test");
        assertEquals(1, wordStorage.getWordStatistics().size());

        try {
            wordStorage.getWordStatistics().remove("test");
            fail("wordStatistics isn't unmodifiable");
        } catch (UnsupportedOperationException e) {
        }
    }
}
