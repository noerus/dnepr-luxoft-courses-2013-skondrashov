package com.luxoft.dnepr.courses.unit1;

import org.junit.Test;

import java.util.Arrays;

import static com.luxoft.dnepr.courses.unit1.LuxoftUtils.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class LuxoftUtilsTest {
    @Test
    public void getMonthNameTest() {
        assertEquals("January", getMonthName(1, "en"));
        assertEquals("February", getMonthName(2, "en"));
        assertEquals("March", getMonthName(3, "en"));
        assertEquals("April", getMonthName(4, "en"));
        assertEquals("May", getMonthName(5, "en"));
        assertEquals("June", getMonthName(6, "en"));
        assertEquals("July", getMonthName(7, "en"));
        assertEquals("August", getMonthName(8, "en"));
        assertEquals("September", getMonthName(9, "en"));
        assertEquals("October", getMonthName(10, "en"));
        assertEquals("November", getMonthName(11, "en"));
        assertEquals("December", getMonthName(12, "en"));

        assertEquals("Январь", getMonthName(1, "ru"));
        assertEquals("Февраль", getMonthName(2, "ru"));
        assertEquals("Март", getMonthName(3, "ru"));
        assertEquals("Апрель", getMonthName(4, "ru"));
        assertEquals("Май", getMonthName(5, "ru"));
        assertEquals("Июнь", getMonthName(6, "ru"));
        assertEquals("Июль", getMonthName(7, "ru"));
        assertEquals("Август", getMonthName(8, "ru"));
        assertEquals("Сентябрь", getMonthName(9, "ru"));
        assertEquals("Октябрь", getMonthName(10, "ru"));
        assertEquals("Ноябрь", getMonthName(11, "ru"));
        assertEquals("Декабрь", getMonthName(12, "ru"));

        assertEquals("Unknown Language", getMonthName(5, "hindi"));
        assertEquals("Unknown Language", getMonthName(13, "hindi"));
        assertEquals("Неизвестный месяц", getMonthName(-5, "ru"));
    }

    @Test(expected = NullPointerException.class)
    public void getMonthNameTestNullPointer() {
        getMonthName(0, null);
    }

    @Test
    public void binaryToDecimalTest() {
        assertEquals("0", binaryToDecimal("000"));
        assertEquals("1", binaryToDecimal("1"));
        assertEquals("1859", binaryToDecimal("11101000011"));
        assertEquals("65535", binaryToDecimal("1111111111111111"));
        assertEquals("Not binary", binaryToDecimal("hello"));
    }

    @Test
    public void decimalToBinaryTest() {
        assertEquals("0", decimalToBinary("0"));
        assertEquals("1", decimalToBinary("1"));
        assertEquals("10", decimalToBinary("002"));
        assertEquals("11101000011", decimalToBinary("1859"));
        assertEquals("Not decimal", decimalToBinary("hello"));
    }

    @Test
    public void sortArrayTest() {
        int[] array = new int[]{5, 7, 4, 6, 9, 8, 1, 2, 3};

        int[] arr = Arrays.copyOf(array, array.length);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, sortArray(arr, true));
        assertArrayEquals(array, arr);

        System.arraycopy(array, 0, arr, 0, array.length);
        assertArrayEquals(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1}, sortArray(arr, false));
        assertArrayEquals(array, arr);
    }
}
