package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.AbstractDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeDaoImplTest {

    // изменил с IDao на AbstractDao
// добавил в AbstractDao геттер хранилища
// и заменил везде в тестах EntityStorage на dao.getStorage()
    AbstractDao<Employee> dao = new EmployeeDaoImpl();

    @Before
    public void init() {
        dao.getStorage().getEntities().clear();
        dao.getStorage().getEntities().put(1L, createEmployee(1L, 1));
        dao.getStorage().getEntities().put(2L, createEmployee(2L, 2));
        dao.getStorage().getEntities().put(3L, createEmployee(3L, 3));
        dao.getStorage().getEntities().put(4L, createEmployee(4L, 4));
        dao.getStorage().getEntities().put(5L, createEmployee(5L, 5));
    }

    @Test
    public void saveBasicTest() throws UserAlreadyExist {
        Employee emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(6), emp.getId());

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(7), emp.getId());

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(8), emp.getId());
    }

    @Test
    public void saveComplicatedTest() throws UserAlreadyExist {
        Employee emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(6), emp.getId());

        dao.getStorage().getEntities().remove(1L);

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(7), emp.getId());

        dao.getStorage().getEntities().remove(2L);
        dao.getStorage().getEntities().remove(3L);

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(8), emp.getId());

        dao.getStorage().getEntities().remove(7L);
        dao.getStorage().getEntities().remove(8L);

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(7), emp.getId());
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveAlreadyExistsTest() throws UserAlreadyExist {
        Employee emp = createEmployee(1L, 1);
        dao.save(emp);
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveAlreadyExists2Test() throws UserAlreadyExist {
        Employee emp = createEmployee(5L, 1);
        dao.save(emp);
    }

    @Test
    public void updateOkTest() throws UserNotFound {
        Employee emp = createEmployee(1L, 10);
        emp = dao.update(emp);
        Assert.assertEquals(new Long(1), emp.getId());
        Assert.assertEquals(10, emp.getSalary());
        emp = dao.getStorage().getEntities().get(1L);
        Assert.assertEquals(new Long(1), emp.getId());
        Assert.assertEquals(10, emp.getSalary());
    }

    @Test(expected = UserNotFound.class)
    public void updateFail1Test() throws UserNotFound {
        Employee emp = createEmployee(null, 10);
        emp = dao.update(emp);
    }

    @Test(expected = UserNotFound.class)
    public void updateFail2Test() throws UserNotFound {
        Employee emp = createEmployee(6L, 10);
        emp = dao.update(emp);
    }

    @Test
    public void getTest() {
        Employee emp = dao.get(3);
        Assert.assertEquals(new Long(3), emp.getId());
        Assert.assertEquals(3, emp.getSalary());

        emp = dao.get(10);
        Assert.assertNull(emp);
    }

    @Test
    public void deleteTest() {
        Assert.assertFalse(dao.delete(7L));
        Assert.assertFalse(dao.delete(8L));
        Assert.assertFalse(dao.delete(9L));

        Assert.assertTrue(dao.delete(1L));
        Assert.assertTrue(dao.delete(2L));
        Assert.assertTrue(dao.delete(3L));
        Assert.assertTrue(dao.delete(4L));
        Assert.assertTrue(dao.delete(5L));

        Assert.assertFalse(dao.delete(1L));
        Assert.assertFalse(dao.delete(2L));
    }

    @Test
    public void redisTest() {
        IDao<Redis> redisDao = new RedisDaoImpl();
        Redis redis = new Redis();
        redis.setId(123L);
        redis.setWeight(100);
        redisDao.save(redis);
        Redis res = redisDao.get(123);
        Assert.assertNotNull(res);
        Assert.assertEquals(100, res.getWeight());
    }

    private static Employee createEmployee(Long id, int weight) {
        Employee emp = new Employee();
        emp.setId(id);
        emp.setSalary(weight);
        return emp;
    }
}