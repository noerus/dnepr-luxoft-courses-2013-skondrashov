package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.luxoft.dnepr.courses.unit2.LuxoftUtils.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class LuxoftUtilsTest {
    @Test
    public void sortArrayTest() {
        String[] baseArray = new String[]{"b", "B", "z", "Z", "a", "hEll", "A", "aaa", "aa aa", "HeLLo", "aa"};
        String[] sortedArrayAsc = new String[]{"A", "B", "HeLLo", "Z", "a", "aa", "aa aa", "aaa", "b", "hEll", "z"};
        String[] sortedArrayDesc = new String[]{"z", "hEll", "b", "aaa", "aa aa", "aa", "a", "Z", "HeLLo", "B", "A"};

        String[] arr = Arrays.copyOf(baseArray, baseArray.length);
        assertArrayEquals(sortedArrayAsc, sortArray(arr, true));
        assertArrayEquals(baseArray, arr);

        assertArrayEquals(sortedArrayDesc, sortArray(arr, false));
        assertArrayEquals(baseArray, arr);
    }

    @Test
    public void wordAverageLengthTest() {
        assertEquals(2.25, wordAverageLength("I have a cat"), Double.MIN_VALUE);
        assertEquals(0, wordAverageLength(""), Double.MIN_VALUE);
        assertEquals(1.0, wordAverageLength("a"), Double.MIN_VALUE);
        assertEquals(3.0, wordAverageLength("one"), Double.MIN_VALUE);
        assertEquals(3.0, wordAverageLength("one two"), Double.MIN_VALUE);
        assertEquals(11.0 / 3.0, wordAverageLength("one two three"), Double.MIN_VALUE);
    }

    @Test
    public void reverseWordsTest() {
        assertEquals("cba trf rgrg ", reverseWords("abc frt grgr "));
        assertEquals("", reverseWords(""));
        assertEquals("cba", reverseWords("abc"));
        assertEquals("cba    fed", reverseWords("abc    def"));
        assertEquals("cba  \t fed hg", reverseWords("abc  \t def gh"));
        assertEquals("  olleH dlroW ", reverseWords("  Hello World "));
    }

    @Test
    public void getCharEntriesTest() {
        assertArrayEquals(new char[]{'a', 'I', 'c', 'e', 'h', 't', 'v'}, getCharEntries("I have a cat"));
        assertArrayEquals(new char[]{}, getCharEntries(""));
        assertArrayEquals(new char[]{'a'}, getCharEntries("a"));
        assertArrayEquals(new char[]{'b', 'a'}, getCharEntries("aabbb"));
        assertArrayEquals(new char[]{'l', 'o', 'H', 'W', 'd', 'e', 'r'}, getCharEntries("Hello World!!!"));
    }

    @Test
    public void calculateOverallAreaTest() {
        List<Figure> figures = new LinkedList<>();
        figures.add(new Square(1));
        figures.add(new Circle(1));
        figures.add(new Hexagon(1));
        assertEquals(1. + 3.14159265 + 2.59807621, calculateOverallArea(figures), 0.000001);
    }
}
