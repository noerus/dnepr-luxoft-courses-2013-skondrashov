package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class BankTest {
    private Bank bank;

    @Before
    public void setUp() {
        try {
            new Bank(null);
            fail();
        } catch (Throwable e) {
            if (e.getClass() != IllegalArgumentException.class) fail();
        }
        try {
            new Bank("");
            fail();
        } catch (Throwable e) {
            if (e.getClass() != IllegalJavaVersionError.class) fail();
        }
        try {
            new Bank(" ");
            fail();
        } catch (Throwable e) {
            if (e.getClass() != IllegalJavaVersionError.class) fail();
        }
        try {
            new Bank("3");
            fail();
        } catch (Throwable e) {
            if (e.getClass() != IllegalJavaVersionError.class) fail();
        }

        try {
            bank = new Bank("1.7");
        } catch (Throwable e) {
            try {
                bank = new Bank("1.6");
            } catch (Throwable t) {
                fail();
            }
        }
    }

    @Test
    public void makeMoneyTransactionTest() {
        Map<Long, UserInterface> map = new HashMap<>();
        User u1 = new User(1L, "User 1", new Wallet(101L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO));
        User u2 = new User(2L, "User 2", new Wallet(102L, BigDecimal.ZERO, WalletStatus.BLOCKED, BigDecimal.ZERO));
        map.put(u1.getId(), u1);
        map.put(u2.getId(), u2);
        bank.setUsers(map);

        try {
            bank.makeMoneyTransaction(0L, 1L, BigDecimal.ZERO);
            fail();
        } catch (NoUserFoundException e) {
            //good
        } catch (Throwable e) {
            fail(e.toString());
        }

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ZERO);
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'User 1' wallet is blocked", e.getMessage());
        } catch (Throwable e) {
            fail(e.toString());
        }

        bank.getUsers().get(1L).getWallet().setStatus(WalletStatus.ACTIVE);

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ZERO);
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'User 2' wallet is blocked", e.getMessage());
        } catch (Throwable e) {
            fail(e.toString());
        }

        bank.getUsers().get(2L).getWallet().setStatus(WalletStatus.ACTIVE);

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ZERO);
        } catch (Throwable e) {
            fail(e.toString());
        }

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ONE);
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'User 1' has insufficient funds (0.00 < 1.00)", e.getMessage());
        } catch (Throwable e) {
            fail(e.toString());
        }

        bank.getUsers().get(1L).getWallet().setAmount(BigDecimal.ONE);

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ONE);
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'User 2' wallet limit exceeded (0.00 + 1.00 > 0.00)", e.getMessage());
        } catch (Throwable e) {
            fail(e.toString());
        }

        bank.getUsers().get(2L).getWallet().setMaxAmount(BigDecimal.ONE);

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ONE);
        } catch (Throwable e) {
            fail(e.toString());
        }

        assertEquals(BigDecimal.ZERO, bank.getUsers().get(1L).getWallet().getAmount());
        assertEquals(BigDecimal.ONE, bank.getUsers().get(2L).getWallet().getAmount());

        User u3 = new User(3L, "Anton", new Wallet(103L, BigDecimal.valueOf(10), WalletStatus.ACTIVE, BigDecimal.valueOf(10.99)));
        bank.getUsers().put(u3.getId(), u3);

        try {
            bank.makeMoneyTransaction(2L, 3L, BigDecimal.valueOf(1));
            fail();
        } catch (TransactionException e) {
            assertEquals("User 'Anton' wallet limit exceeded (10.00 + 1.00 > 10.99)", e.getMessage());
        } catch (Throwable e) {
            fail(e.toString());
        }

        bank.setUsers(null);

        try {
            bank.makeMoneyTransaction(1L, 2L, BigDecimal.ZERO);
            fail();
        } catch (Throwable e) {
            if (e.getClass() != IllegalStateException.class) fail(e.toString());
        }
    }
}
