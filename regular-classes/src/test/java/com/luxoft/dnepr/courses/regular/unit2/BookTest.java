package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws CloneNotSupportedException {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();

        assertEquals(book.getCode(), cloned.getCode());
        assertEquals(book.getName(), cloned.getName());
        assertEquals(book.getPrice(), cloned.getPrice(), 0.000001);
        assertEquals(book.getPublicationDate(), cloned.getPublicationDate());
        assertNotSame(book.getPublicationDate(), cloned.getPublicationDate());
    }

    @Test
    public void testEquals() {
        Book one = productFactory.createBook("code", "Thinking in Java", 111, new GregorianCalendar(2006, 0, 1).getTime());
        Book two = productFactory.createBook("code", "Thinking in Java", 222, new GregorianCalendar(2006, 0, 1).getTime());
        Book three = productFactory.createBook("code", "Thinking in Java", 333, new GregorianCalendar(2006, 0, 1).getTime());

        assertTrue(one.equals(one));
        assertTrue(one.equals(two) && two.equals(one));
        assertTrue(one.equals(two) && two.equals(three) && one.equals(three));
        assertFalse(one.equals(null));

        Book four = productFactory.createBook("code", "asdf", 100, new GregorianCalendar(2016, 5, 1).getTime());
        assertFalse(one.equals(four));
    }

    @Test
    public void testHashCode() {
        Book one = productFactory.createBook("code", "Thinking in Java", 111, new GregorianCalendar(2006, 0, 1).getTime());
        Book two = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book three = productFactory.createBook("abc", "qwer", 200, new GregorianCalendar(2006, 0, 1).getTime());

        assertEquals(one, two);
        assertEquals(one.hashCode(), two.hashCode());
        assertNotEquals(one, three);
        assertNotEquals(one.hashCode(), three.hashCode());
    }
}
