package com.luxoft.dnepr.courses.regular.unit3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class UserTest {
    @Test
    public void userTest() {
        User user = new User();

        Wallet wallet = new Wallet();
        String name = "Name Asdf Qwer ";
        Long id = 12345L;

        user.setId(id);
        user.setName(name);
        user.setWallet(wallet);

        assertEquals(id, user.getId());
        assertEquals(name, user.getName());
        assertSame(wallet, user.getWallet());
    }
}
